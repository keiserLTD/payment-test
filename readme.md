# Постановка #

Задание:
Необходимо разработать веб-приложение простой платежной системы. Требования:

1) В системe клиент соотносится с "кошельком", содержащим денежные средства в некой валюте.
Сохраняется информация о имени клиента, стране и городе его регистрации, валюте кошелька и
остатке средств на нем.
2) Клиенты могут делать друг другу денежные переводы в валюте получателя или валюте
отправителя.
3) Сохраняется информация о всех операциях на кошельке клиента.
4) В системе существует информация о курсах валют (для всех зарегистрированных кошельков) к
USD на каждый день.
5) Проект представляет из себя 2 основных части - HTTP REST API и страница с отчетом.
6) HTTP API должен представлять следующие интерфейсы:
    1) регистрация клиента с указанием его имени, страны, города регистрации, валюты
создаваемого кошелька.
    2) зачисление денежных средств на кошелек клиента
    3) перевод денежных средств с одного кошелька на другой.
    4) загрузка котировки валюты к USD на дату
7) Отчет должен отображать историю всех операций по кошельку указанного клиента за период.
    1) Параметры: Имя клиента (обязательный параметр), Начало периода (необязательный
параметр), конец периода (необязательный параметр).
    2) Необходимо также вывести общую сумму операций по счету за период в USD и валюте счета
    3) Должна быть предусмотрена возможность скачивания результатов отчета в файл (например,
в CSV или XML формате).

Примечания:
1) Авторизация/аутентификация на любой из частей сайта не обязательна. Все данные о
пользователях, там, где это нужно, могут приходить в теле запроса одним из параметров.
2) При решении должна использоваться реляционная СУБД.
3) При отправке решения по email укажите свою фамилию в теме письма

# Используемые технологии #

PHP framework Laravel 5.6, MySQL 5.7, PHP 7.1, Redis 3.2, для быстрой реализации фронтенд составляющей - bootstrap 3, jquery 3.3

Работа с базой данных ведётся и спользованием встроенной ORM Eloquent, реализованные модели помещены в namespace app\Models.
Работа с моделями вынесена в namespace app\Repositories.
Работа с операциями транзакций вынесена в отдельную директорию app\Payment.

# Структура базы данных #
Все суммы касающиеся денежных среддств хранятся в целочисленных значениях (int), ибзегаем работы с decimal типом.
При сохранении умножаем на 100 для избавления от дробной части, при отображении пользователю делим.
Подобный подход позволяет снизить нагрузку на базу данных и повысить точность расчетов на стороне приложения (работать с int всегда проще).

В ряде случаев использвались беззнаковые поля, для охвата большего диапазона значений, когда заранее известно что отрицательных значений быть не может.

+ Сущность клиента **clients** содержит общую информацию о клиентах
```
client_id - первичный ключ
name - уникальное имя
country - страна
city - город
```

+ Сущность валюты **currency_list** содержит информацию о всех возможных валютах в системе
```
currency_id - первичный ключ
currency_name - уникальное имя валюты (название)
currency - международное обозначение (RUB/EUR etc), уникальный индекс
factor - кратность валюты
date_update - дата обновления
```

+ Сущность курса обмена валюты **currency_rates** содержит информацию о возможных валютах в системе
```
rate_id - первичный ключ
rate - курс валюты к USD
update_date - дата последнего обновления
rate_date - дата курса
currency_id - внешний ключ таблицы currency_list
```

+ Сущность кошелька клиента **wallets**

```
wallet_id - первичный ключ
client_id - внешний ключ таблицы клиенты
currency_id - внешний ключ таблицы валют
amount - баланс (default 0)
```

+ Сущность истории операций **wallets_history**
```
source_wallet_id - исходный кошелёк, с которого происходит транзакция (в случае если null - то идёт пополнение баланса кошелька target_wallet_id)
target_wallet_id - кошелёк на который происходит перевод
amount - сумма операции
usd_amount - сумма операции в USD
date - дата операции
```

Миграции расположены в папке database\migrations, раскидал на мой взгляд необходимые индексы.
В дирректории seeds расположен скрипт для первичного заполнения базы тестовыми данными.

# API #

Обработка всех запросов находится в контроллере ApiController. В случае успешного запроса система вернет код 200, в случае ошибок валидации 400 и текст ошибки в формате JSON

+	Регистрация нового клиента осуществляется передачей PUT-запроса по адресу /api/client 
    В теле запроса необходимо передать параметры name, country, city, currency.
	Данный запрос вызовет функцию clientAdd. Будут заполнены данные о клиенте в сущностях client и создана заапись в таблице wallet.
	
+	Загрузка котировок осуществляется передачей PUT-запроса по адресу /api/exchange_rate. 
    В теле запроса необходимо передать названия валюты (currency), значение котировки (rate) и дату котировки (**rate_date**, формат ‘Y-m-d’).

+	Пополнение счета осуществляется передачей POST-запроса по адресам /api/wallet/**{wallet_id}**. 
    Пополнение баланса происходит но номеру кошелька
    В теле запроса необходимо передать сумму пополнения (**amount**) в валюте счета.
    В данном случае будет вызван метод singleWalletTransaction, в котором после проверок будет инициализирован объект транзакции \App\Payment\TransactionObject и передан в очередь на обработку.
    Испольуется механизм очередей laravel, в качестве хранилища очередей испольуется redis и механизм сериализации/десеарилизации объектов (реализован в обработчике TransactionWorker) 

+	Перевод между кошельками осуществляется передачей POST-запроса по адресам /api/wallet/**{wallet_from_id}**/**{wallet_to_id}**
    В теле запроса необходимо передать сумму пополнения (**amount**) и в валюте какого счета производится операция currency
	Данный запрос вызовет функцию walletsTransaction, механика обработки очереди такая же как и в случае с пополнением.

# Обработка очереди транзакций #

Все процессы реализованы через очереди Laravel, хранилище очередей redis, их описание располагается в App/Jobs/. 
Для запуска процесса обработки очереди можем запустить команду:
```
php artisan queue:work redis --queue=wallet_transactions
```
Для удобства управлления процессами обработчиками рекомендуется использование любого менеджера процессов (laravel да и я предпочёл бы supervisor)

# Отчет #
Отчет доступен на главной странице приложения.
Для выгрузки использовался формат CSV, метод IndexController::exportToCsv