<?php

namespace App\Http\Controllers;

use App\Models\ClientsModel;
use App\Models\CurrencyListModel;
use App\Repositories\ClientsRepository;
use App\Repositories\WalletRepository;
use App\Repositories\WalletsHistoryRepository;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function main()
    {
        $clients = ClientsRepository::getClients();

        return view('clients.index', [
            'clients' => $clients,
            'client_id' => null,
            'date_from' => '',
            'date_to' => '',
            'wallet_history' => null,
        ]);
    }

    public function report(Request $request, $clientId)
    {
        $client = ClientsModel::find($clientId);

        if (is_null($client)) {
            return response('Client not exists', 400)
                ->header('Content-Type', 'text/plain');
        }

        $wallet = WalletRepository::getWalletForClient($client->client_id);

        $clients = ClientsRepository::getClients($wallet, $request);
        $currency = CurrencyListModel::find($wallet->currency_id);
        $walletHistory = WalletsHistoryRepository::getWalletHist($wallet->wallet_id
            , $this->convertDate($request->date_from)
            , $this->convertDate($request->date_to)
            , $currency
        );

        return view('clients.index', [
            'client' => $client,
            'client_id' => $client->client_id,
            'clients' => $clients,
            'currency' => $currency,
            'wallet_history' => $walletHistory,
            'sum_usd' => $walletHistory->sum('usd_amount'),
            'sum_amount' => $walletHistory->sum('amount'),
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
        ]);
    }

    public function exportToCsv(Request $request, $clientId)
    {
        $client = ClientsModel::find($clientId);
        $wallet = WalletRepository::getWalletForClient($client->client_id);
        $currency = CurrencyListModel::find($wallet->currency_id);
        $walletHistory = WalletsHistoryRepository::getWalletHist($wallet->wallet_id
            , $this->convertDate($request->date_from)
            , $this->convertDate($request->date_to)
            , $currency
        );

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=report.csv",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Pragma" => "no-cache",
            "Expires" => "0"
        ];

        $columns = ['Date', 'Amount', 'USD', 'Operation'];

        $callback = function () use ($walletHistory, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ';');

            foreach ($walletHistory as $item) {
                fputcsv($file, [
                    $item->p_date,
                    $item->p_amount,
                    $item->p_usd,
                    $item->p_operation
                ],
                    ';'
                );
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    private function convertDate($date)
    {
        return $date == '' ? '' : date('Y-m-d H:i', strtotime($date));
    }
}
