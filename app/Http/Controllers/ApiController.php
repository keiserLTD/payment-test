<?php

namespace App\Http\Controllers;

use App\Jobs\TransactionWorker;
use App\Models\ClientsModel;
use App\Models\CurrencyRatesModel;
use App\Models\WalletsModel;
use App\Payment\TransactionObject;
use App\Payment\WalletRefill;
use App\Payment\WalletsTransaction;
use App\Repositories\CurrencyRepository;
use App\Repositories\WalletsHistoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clientAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:clients|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            #'currency' => 'required|exists:currency_list,currency',
            'currency' => 'required',
        ]);

        $arResponce = [
            'data' => '',
            'errors' => [],
        ];

        $statusCode = 400;

        DB::transaction(function () use ($request, $validator, &$arResponce, &$statusCode) {

            if ( ! $existingCurrency = CurrencyRepository::getCurrency($request->get('currency'))) {
                $arResponce['errors'] = 'No currency found';
            } else {
                if ($validator->fails()) {
                    $arResponce['errors'] = $validator->errors();
                } else {
                    $newClientId = ClientsModel::create($request->except('currency'));

                    if ($newClientId->client_id) {
                        WalletsModel::create([
                            'client_id' => $newClientId->client_id,
                            'currency_id' => $existingCurrency->currency_id,
                        ]);
                    }

                    $arResponce['data'] = $newClientId;

                    $statusCode = 200;
                }
            }

        }, 5);

        return response()->json($arResponce, $statusCode);
    }


    public function exchangeRate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:3',
            'rate' => 'required|numeric',
            'date' => 'required|date|after:now',
        ]);

        if ($validator->fails()) {
            return $this->getReturnResponce($validator->messages());
        }

        $currency = CurrencyRepository::getCurrency($request->get('currency'));

        if (is_null($currency)) {
            return $this->getReturnResponce('Currency not exists');
        }

        $rateModel = CurrencyRatesModel::firstOrNew([
            'currency_id' => $currency->currency_id,
            'rate_date' => date('Y-m-d')
        ]);

        if ( ! $rateModel->exists()) {
            $rateModel->currency_id = $currency->currency_id;
        }

        $rateModel->rate = $request->get('rate');
        $rateModel->rate_date = $request->get('date');
        $rateModel->update_date = date('Y-m-d H:i:s');

        $rateModel->save();

        return $this->getReturnResponce('ok', 200);
    }

    /**
     * @param Request $request
     * @param $walletId
     * @return mixed
     */
    public function singleWalletTransaction(Request $request, $walletId)
    {
        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:3',
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return $this->getReturnResponce($validator->messages());
        }

        $currency = CurrencyRepository::getCurrency($request->get('currency'));
        $wallet = WalletsModel::find($walletId);

        if ( ! $currency) {
            return $this->getReturnResponce('Currency is not found');
        }

        if ($currency->currency_id !== $wallet->currency_id) {
            return $this->getReturnResponce('Currency is not supported by this wallet');
        }

        $amount = $request->get('amount');

        if ($amount <= 0) {
            return $this->getReturnResponce('Refill operation needs a positive amount');
        }

        $transObject = (new TransactionObject())
            ->setTo($wallet)
            ->setAmount($request->get('amount'))
            ->setCurrency($currency)
            ->setDate();

        $wt = new WalletsTransaction($transObject);

        TransactionWorker::dispatch($wt)
            ->onQueue('wallet_transactions')
            ->onConnection('redis');

        return $this->getReturnResponce('ok', 200);
    }

    /**
     * @param Request $request
     * @param $walletFromId
     * @param $walletToId
     * @return mixed
     * @throws \Exception
     */
    public function walletsTransaction(Request $request, $walletFromId, $walletToId)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'currency' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->getReturnResponce($validator->messages());
        }

        $walletFrom = WalletsModel::find($walletFromId);
        $walletTo = WalletsModel::find($walletToId);

        if (!$walletFrom || ! $walletTo) {
            return $this->getReturnResponce('No wallet found');
        }

        $amount = $request->get('amount');

        $currency = CurrencyRepository::getCurrency($request->get('currency'));

        if(!$currency) {
            return $this->getReturnResponce('No currency found');
        }

        $transObject = (new TransactionObject())
            ->setFrom($walletFrom)
            ->setTo($walletTo)
            ->setAmount($amount)
            ->setCurrency($currency)
            ->setDate();

        TransactionWorker::dispatch($transObject)
            ->onQueue('wallet_transactions')
            ->onConnection('redis');

        return $this->getReturnResponce('ok', 200);

    }


    /**
     * @param $mess
     * @param int $code
     * @return mixed
     */
    private function getReturnResponce($mess, $code = 400)
    {
        return response()->json([
            'responce' => $mess
        ], $code);
    }

}
