<?php

namespace App\Payment;


interface QueueTransactionInterface
{
    public function run();
}