<?php

namespace App\Payment;


class TransactionObject
{
    private $from = null;

    private $to;

    private $date;

    private $currency;

    private $amount;

    private $usd = 0;

    /**
     * @return int
     */
    public function getUsd(): int
    {
        return $this->usd;
    }

    /**
     * @param int $usd
     * @return TransactionObject
     */
    public function setUsd(int $usd): TransactionObject
    {
        $this->usd = $usd;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return TransactionObject
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return TransactionObject
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     * @return TransactionObject
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    public function setDate()
    {
        $this->date = date('Y-m-d');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return TransactionObject
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }


}