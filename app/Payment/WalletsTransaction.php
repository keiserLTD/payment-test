<?php

namespace App\Payment;


use App\Models\CurrencyListModel;
use App\Models\WalletsModel;
use App\Repositories\CurrencyRatesRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\WalletRepository;
use App\Repositories\WalletsHistoryRepository;
use Illuminate\Support\Facades\DB;

class WalletsTransaction implements QueueTransactionInterface
{

    private $trans;

    /**
     * WalletsTransaction constructor.
     * @param TransactionObject $object
     */
    public function __construct(TransactionObject $object)
    {
        $this->trans = $object;
    }

    private function getWalletAmount(WalletsModel $wallet, $transAmount, $usd, $rate)
    {
        if ($wallet->currency_id == $this->trans->getCurrency()->currency_id) {
            return $transAmount;
        }

        $walletCurrencyFactor = CurrencyRepository::getFactorById($wallet->currency_id);
        return ceil($usd * $rate->rate * $walletCurrencyFactor);
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $wf = $this->trans->getFrom();
        $wTo = $this->trans->getTo();
        $currency = $this->trans->getCurrency();
        $transDate = $this->trans->getDate();
        $transAmount = $this->trans->getAmount();

        $rate = CurrencyRatesRepository::getRate($currency->currency_id, $transDate);

        $usd = floor(100 * $transAmount / $rate->rate);
        $transCurrency = CurrencyListModel::find($currency->currency_id);
        $transAmount = floor($transAmount * $transCurrency->factor);

        DB::beginTransaction();

        $this->trans->setUsd($usd);

        if (!is_null($wf)) {
            $fromAmount = -1 * $this->getWalletAmount($wf, $transAmount, $usd, $rate);
            WalletRepository::changeBalance($wf, $fromAmount);
            WalletsHistoryRepository::logTransaction($wf, $wTo, $fromAmount, $usd);
        }

        $toAmount = $this->getWalletAmount($wTo, $transAmount, $usd, $rate);

        WalletRepository::changeBalance($wTo, $toAmount);
        WalletsHistoryRepository::logTransaction(null, $wTo, $toAmount, $usd);

        DB::commit();
    }
}