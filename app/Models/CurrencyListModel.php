<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyListModel extends Model
{
    public $timestamps = false;

    protected $table = 'currency_list';

    public $primaryKey = 'currency_id';

    protected $fillable = [
        'currency_name',
        'factor',
        'currency',
    ];

    public static function getCurrency($currency)
    {
        return CurrencyListModel::where('currency', $currency)->first();
    }

}
