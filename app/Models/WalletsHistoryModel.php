<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletsHistoryModel extends Model
{
    protected $table = 'wallets_history';

    public $primaryKey = 'client_id';

    public $timestamps = false;

    protected $fillable = [
        'target_wallet_id',
        'source_wallet_id',
        'amount',
        'usd_amount',
        'date',
    ];
}
