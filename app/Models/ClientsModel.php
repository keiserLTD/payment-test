<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientsModel extends Model
{
    protected $table = 'clients';

    public $timestamps = false;

    protected $guarded  = ['id'];

    public $primaryKey = 'client_id';

    public function wallet()
    {
        return $this->hasOne(WalletsModel::class, 'client_id', 'client_id');
    }
}
