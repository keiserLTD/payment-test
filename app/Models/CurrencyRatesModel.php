<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyRatesModel extends Model
{

    public $timestamps = false;

    protected $table = 'currency_rates';

    public $primaryKey = 'rate_id';

    protected $guarded = [
        'rate_id',
    ];

}
