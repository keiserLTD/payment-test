<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletsModel extends Model
{
    protected $table = 'wallets';

    public $primaryKey = 'wallet_id';

    public $timestamps = false;

    protected $fillable = ['client_id', 'currency_id'];

    public function client()
    {
        return $this->belongsTo(ClientsModel::class, 'client_id', 'client_id');
    }

    public function currencyDict()
    {
        return $this->hasOne(CurrencyListModel::class, 'currency_id', 'currency_id');
    }

    public function walletHistory()
    {
        return $this->hasMany(WalletsHistoryModel::class, 'wallet_id', 'wallet_id');
    }
}
