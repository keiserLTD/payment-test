<?php

namespace App\Jobs;

use App\Payment\QueueTransactionInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class TransactionWorker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transaction;

    /**
     * Create a new job instance.
     *
     * @param $transaction
     */
    public function __construct(QueueTransactionInterface $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->attempts() > 5) {
            $this->delete();
            Log::critical('Problem with transaction ' . serialize($this->transaction));
        }

        $this->transaction->run();
    }


    public function failed(\Exception $exception)
    {
        Log::critical($exception->getMessage());
    }

}
