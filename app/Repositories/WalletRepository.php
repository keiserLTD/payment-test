<?php

namespace App\Repositories;


use App\Models\WalletsModel;
use Illuminate\Support\Facades\DB;

class WalletRepository
{

    public static function getWalletForClient($client_id)
    {
        return $wallet = WalletsModel::where('client_id', $client_id)->first();
    }

    public static function changeBalance(WalletsModel $wallet, $amount)
    {
        DB::table('wallets')
            ->where('wallet_id', $wallet->wallet_id)
            ->increment('amount', $amount);
    }

}