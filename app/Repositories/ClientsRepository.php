<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\ClientsModel;

class ClientsRepository
{
    public static function getClientsModel($name)
    {
        return ClientsModel::where('name', $name)->first();
    }

    public static function getClients()
    {
        return ClientsModel::orderBy('name', 'asc')->get();
    }
}
