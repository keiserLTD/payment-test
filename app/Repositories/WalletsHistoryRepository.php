<?php

namespace App\Repositories;

use App\Models\WalletsHistoryModel;
use App\Models\WalletsModel;

class WalletsHistoryRepository
{
    /**
     * @param WalletsModel|null $from
     * @param WalletsModel $to
     * @param int $amount
     * @param int $usdAmount
     * @param $date
     * @return mixed
     */
    public static function logTransaction($from = null, $to, int $amount = 0, int $usdAmount = 0)
    {
        return WalletsHistoryModel::create([
            'source_wallet_id' => ! is_null($from) ? $from->wallet_id : null,
            'target_wallet_id' => $to->wallet_id,
            'amount' => abs($amount),
            'usd_amount' => $usdAmount,
            'date' => date('Y-m-d H:i:s'),
        ]);
    }

    public static function getWalletHist($wallet_id, $dateFrom = null, $dateTo = null, $currency)
    {
        if ($dateFrom != '' && $dateTo != '') {
            $walletHistory = WalletsHistoryModel::where('source_wallet_id', $wallet_id)
                ->orWhere('target_wallet_id', $wallet_id)
                ->whereBetween('date', [$dateFrom, $dateTo]);
        } else if ($dateFrom != '') {
            $walletHistory = WalletsHistoryModel::where('source_wallet_id', $wallet_id)
                ->orWhere('target_wallet_id', $wallet_id)
                ->where('date', '>=', $dateFrom);
        } else if ($dateTo != '') {
            $walletHistory = WalletsHistoryModel::where('source_wallet_id', $wallet_id)
                ->orWhere('target_wallet_id', $wallet_id)
                ->where('date', '<=', $dateTo);
        } else {
            $walletHistory = WalletsHistoryModel::where('source_wallet_id', $wallet_id)
                ->orWhere('target_wallet_id', $wallet_id);
        }

        $data = $walletHistory->orderBy('date')->get();

        // prepared fields
        foreach ($data as &$elem) {
            $elem->p_date = date('d.m.Y H:i', strtotime($elem->date));
            $elem->p_amount = (!is_null($elem->source_wallet_id) ? '-' : '') . ($elem->amount / $currency->factor);
            $elem->p_usd = (!is_null($elem->source_wallet_id) ? '-' : '') . ($elem->usd_amount / 100);
            $elem->p_operation = is_null($elem->source_wallet_id) ? 'refill' : 'transaction';
        }

        return $data;
    }
}