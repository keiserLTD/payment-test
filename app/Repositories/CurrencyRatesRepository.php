<?php

namespace App\Repositories;


use App\Models\CurrencyRatesModel;

class CurrencyRatesRepository
{

    public static function getRate($currencyId, $rateDate)
    {
        return CurrencyRatesModel::where('currency_id', $currencyId)
            ->where('rate_date', $rateDate)->first();
    }

}