<?php

namespace App\Repositories;

use App\Models\CurrencyListModel;

class CurrencyRepository
{

    public static function getCurrency($currency)
    {
        return CurrencyListModel::where('currency', $currency)->first();
    }

    public static function getFactorById($id)
    {
        return CurrencyListModel::find($id)->factor;
    }

}