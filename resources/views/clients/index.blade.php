@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Report
                </div>

                <div class="panel-body">
                    <form id="filter" action="{{ url('client') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="client_id" class="col-sm-1 control-label">Client</label>

                            <div class="col-sm-3">
                                <select id="client_id" name="client_id" class="form-control">
                                    <option></option>
                                    @foreach ($clients as $option)
                                        <option value="{{ $option->client_id }}" {{ $option->client_id == $client_id ? 'selected' : '' }}>
                                            {{ $option->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="date_from" class="col-sm-1 ">Date from</label>

                            <div class="col-sm-2">
                                <input id="date_from" autocomplete="off" type="text" name="date_from"
                                       class="form-control"
                                       value="{{$date_from }}">
                            </div>

                            <label for="date_to" class="col-sm-1 control-label">Date to</label>

                            <div class="col-sm-2">
                                <input id="date_to" name="date_to" autocomplete="off" type="text" class="form-control"
                                       value="{{ $date_to }}">
                            </div>

                            <div class="col-sm-1">
                                <button id="search" type="button" class="btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search
                                </button>
                            </div>

                            @if (count($wallet_history) > 0)
                                <div class="col-sm-1 form-group">
                                    <p><a href="{{ route('export_csv', ['client_id' => $client->client_id]) }}"
                                          class="btn"><i class="fa fa-btn fa-table"></i> export</a></p>
                                </div>
                            @endif
                        </div>
                    </form>

                    @if (count($wallet_history))
                        <div class="table-responsive">
                            <table id="table_report" class="table table-striped col-sm-10"
                                   caption="{{ $client->name }}">
                                <thead>
                                <tr>
                                    <th colspan="4">
                                        {{ $client->name }}
                                    </th>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>USD</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($wallet_history as $row)
                                    <tr>
                                        <td>{{ $row->p_date }}</td>
                                        <td>{{ $row->p_amount }}</td>
                                        <td>{{ $row->p_usd }}</td>
                                        <td>{{ $row->p_operation }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <th>{{ $sum_amount / $currency->factor }}</th>
                                    <th>{{ $sum_usd / 100 }}</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    @else
                        <p class="col-sm-offset-3 col-sm-6 text-center">No history</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {

            $('#date_from').datepicker({format: 'dd.mm.yyyy'});
            $('#date_to').datepicker({format: 'dd.mm.yyyy'});

            $('#search').click(function () {
                var $form = $('#filter'),
                    action = $form.attr('action'),
                    client_id = $('#client_id option:selected').val();

                $form.attr('action', action + '/' + client_id);
                $form.submit();
            });

        });
    </script>
@endsection
