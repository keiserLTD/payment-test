<?php

use App\Models\CurrencyListModel;
use App\Models\CurrencyRatesModel;
use Illuminate\Database\Seeder;
use App\Models\ClientsModel;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = \Faker\Factory::create();

        $this->addCurrencies();
        $this->addClients();
        $this->addRates();
    }

    public function addClients()
    {
        ClientsModel::truncate();

        for ($i = 0; $i < 100; $i++) {
            $clientID = ClientsModel::create([
                'name' => $this->faker->unique()->name,
                'country' => $this->faker->country,
                'city' => $this->faker->city,
            ])->client_id;

            \App\Models\WalletsModel::create(
                [
                    'client_id' => $clientID,
                    'currency_id' => rand(1,3),
                    'amount' => rand(0, 100000)
                ]
            );
        }
    }

    public function addCurrencies()
    {
        $arCurrencies = [
            ['currency_name' => 'Russian ruble', 'currency' => 'RUB', 'factor' => '100'],
            ['currency_name' => 'Euro', 'currency' => 'EUR', 'factor' => '100'],
            ['currency_name' => 'Czech krona', 'currency' => 'CZK', 'factor' => '100'],
        ];

        $date = date('Y-m-d H:i:s');
        foreach ($arCurrencies as $index => $arCurrency) {
            \App\Models\CurrencyListModel::create($arCurrency + ['date_update' => $date]);
        }
    }


    public function addRates()
    {
        $currencyList = \App\Models\CurrencyListModel::all();

        $rate = [];
        $date = date('Y-m-d H:i:s');
        foreach ($currencyList as $key => $currency) {
            $rate[] = [
                'rate' => (1000 - rand(1, 999)) / 100,
                'currency_id' => $currency->currency_id,
                'update_date' => $date,
            ];
        }

        CurrencyRatesModel::truncate();

        for ($i = -30; $i < 30; $i++) {
            foreach ($rate as &$cur) {
                $cur['rate_date'] = date('Y-m-d', strtotime(($i >= 0 ? "+" : "") . $i . " days"));
                $cur['rate'] = round($cur['rate'] * (rand(90, 110) / 100), 2);
            }

            CurrencyRatesModel::insert($rate);
        }
    }

}
