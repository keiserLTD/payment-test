wallets_history<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WalletsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets_history', function (Blueprint $table) {
            $table->unsignedInteger('source_wallet_id')->nullable();
            $table->unsignedInteger('target_wallet_id');
            $table->bigInteger('amount');
            $table->bigInteger('usd_amount');
            $table->dateTime('date');

            $table->index('source_wallet_id');
            $table->index('target_wallet_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets_history');
    }
}
