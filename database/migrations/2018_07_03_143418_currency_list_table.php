<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CurrencyListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_list', function (Blueprint $table) {
            $table->increments('currency_id');
            $table->string('currency_name');
            $table->string('currency', 10)->unique();
            $table->unsignedInteger('factor');
            $table->dateTime('date_update');

            $table->index('currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_list');
    }
}
