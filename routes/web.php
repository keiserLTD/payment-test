<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@main');
Route::post('/client/{client_id}', 'IndexController@report');
Route::get('/client/{client_id}', 'IndexController@report');
Route::get(
    '/client/{client_id}/export',
    [
        'as' => 'export_csv',
        'uses' => 'IndexController@exportToCsv'
    ]
);