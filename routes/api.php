<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::put('/client', 'ApiController@clientAdd');
Route::put('/exchange_rate', 'ApiController@exchangeRate');
Route::post('/wallet/{wallet_to}', 'ApiController@singleWalletTransaction');
Route::post('/wallet/{wallet_from}/{wallet_to}', 'ApiController@walletsTransaction')
    ->where(['wallet_from' => '[0-9]+', 'wallet_to' => '[0-9]+']);
